package com.example.activity_vibelab

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.activity_vibelab.databinding.ActivityNewBinding

class NewActivity : AppCompatActivity() {
    lateinit var bindingClass: ActivityNewBinding
    lateinit var viewModel: MyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityNewBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)

        viewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        viewModel.currentColor.observe(this, androidx.lifecycle.Observer {
            bindingClass.tvBackground.setBackgroundColor(it)
        })
        getBackgroundColor()
        bindingClass.textView.text = getInfoAboutPerson()
    }

    fun onClickBackToolbar(view: View) {
        var intent = Intent(this, MainActivity::class.java)
        intent.putExtra("backgroundColor", viewModel.currentColor.value)
        startActivity(intent)
    }

    fun getInfoAboutPerson(): String{
        var age = intent.getIntExtra("age", 0)
        var name = intent.getStringExtra("name")
        var secondName = intent.getStringExtra("secondName")
        var fatherName = intent.getStringExtra("fatherName")
        var hobby = intent.getStringExtra("hobby")
        var sb = StringBuffer()
           return sb.append(name)
                .append(" ")
                .append(secondName)
                .append(" ")
                .append(fatherName)
                .append("\n")
                .append(age)
                .append("\n")
                .append(hobby).toString()
    }
    fun getBackgroundColor() {
        var backgroundColor = intent.getIntExtra("backgroundColor", Color.WHITE)
        bindingClass.tvBackground.setBackgroundColor(backgroundColor)
        viewModel.currentColor.value = backgroundColor
    }
}
