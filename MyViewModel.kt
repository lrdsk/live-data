package com.example.activity_vibelab

import android.graphics.Color
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.*

class MyViewModel: ViewModel() {
    var randomVal = Random()
    var color = Color.argb(255,randomVal.nextInt(256), randomVal.nextInt(256),
        randomVal.nextInt(256))


    val currentColor: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
}
