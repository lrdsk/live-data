package com.example.activity_vibelab

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.example.activity_vibelab.databinding.ActivityInfoBinding

class InfoActivity : AppCompatActivity() {
    lateinit var bindingClass: ActivityInfoBinding
    lateinit var viewModel: MyViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)

        viewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        viewModel.currentColor.observe(this, androidx.lifecycle.Observer {
            bindingClass.tvBackground.setBackgroundColor(it)
        })

        bindingClass.tvText.text = getInfoAboutPerson()
        getBackgroundColor()
    }

    fun onClickBackToolbar(view: View) {
        var intent = Intent(this, MainActivity::class.java)
        intent.putExtra("backgroundColor", viewModel.currentColor.value)
        startActivity(intent)
    }

    fun getInfoAboutPerson(): String {
        var age = intent.getIntExtra("age", 0)
        var name = intent.getStringExtra("name")
        var secondName = intent.getStringExtra("secondName")
        var fatherName = intent.getStringExtra("fatherName")
        var hobby = intent.getStringExtra("hobby")
        var sb = StringBuffer()
        when {
            age > 60 ->
                return sb.append("Ваш возраст: ")
                    .append("$age\n")
                    .append("Вы - старик.\n")
                    .append("Ваше ФИО: ")
                    .append("$secondName $name $fatherName\n")
                    .append("У вас так же есть очень интересное хобби:\n")
                    .append("$hobby\n")
                    .append("Советуем вам больше заниматься спортом!").toString()

            age in 30..60 ->
                return sb.append("Ваш возраст: ")
                    .append("$age\n")
                    .append("Вы - в самом соку.\n")
                    .append("Ваше ФИО: ")
                    .append("$secondName $name $fatherName\n")
                    .append("У вас так же есть очень интересное хобби:\n")
                    .append("$hobby\n")
                    .append("Еще многое можно успеть, дерзайте!").toString()
            age in 18..30 ->
                return sb.append("Ваш возраст: ")
                    .append("$age\n")
                    .append("Молодость все прощает.\n")
                    .append("Ваше ФИО: ")
                    .append("$secondName $name $fatherName\n")
                    .append("У вас так же есть очень интересное хобби:\n")
                    .append("$hobby\n")
                    .append("Еще многое можно успеть, дерзайте!").toString()

            age < 18 -> return sb.append("Ваш возраст: ")
                .append("$age\n")
                .append("Беззаботное юнешество.\n")
                .append("Ваше ФИО: ")
                .append("$secondName $name $fatherName\n")
                .append("У вас так же есть очень интересное хобби:\n")
                .append("$hobby\n")
                .append("Наслаждайся детством!").toString()
            else -> return sb.append("Вы ввели очень странный возраст: ")
                .append("$age\n").toString()
        }

    }

    fun getBackgroundColor() {
        var backgroundColor = intent.getIntExtra("backgroundColor", Color.WHITE)
        bindingClass.tvBackground.setBackgroundColor(backgroundColor)
        viewModel.currentColor.value = backgroundColor
    }
}
