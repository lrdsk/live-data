package com.example.activity_vibelab

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activity_vibelab.databinding.ActivitySplashBinding
import kotlinx.coroutines.delay
import kotlin.concurrent.thread

class SplashActivity : AppCompatActivity() {
    lateinit var bindingClass : ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)
        goToMain()
    }
private fun goToMain() {
    thread {
        Thread.sleep(3000)
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
}
