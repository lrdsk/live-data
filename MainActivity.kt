package com.example.activity_vibelab

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.activity_vibelab.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {
    lateinit var bindingClass: ActivityMainBinding
    lateinit var sharedPref: SharedPreferences
    lateinit var editSaveName: EditText
    lateinit var editSaveSecondName: EditText
    lateinit var editSaveFatherName: EditText
    lateinit var editSaveAge: EditText
    lateinit var editSaveHobby: EditText
    lateinit var viewModel: MyViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingClass = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bindingClass.root)
        viewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        viewModel.currentColor.observe(this, androidx.lifecycle.Observer {
            bindingClass.tvBackground.setBackgroundColor(it)
        })
        getBackgroundColorFromActivity()
        init()
    }

    fun onClickSave(view: View){
        var edit : SharedPreferences.Editor = sharedPref.edit()
        edit.putString("name", editSaveName.text.toString())
        edit.putString("secondName", editSaveSecondName.text.toString())
        edit.putString("fatherName", editSaveFatherName.text.toString())
        edit.putString("age", editSaveAge.text.toString())
        edit.putString("hobby", editSaveHobby.text.toString())
        edit.apply()

    }
    fun init(){
        sharedPref = getSharedPreferences("storageData", MODE_PRIVATE)
        editSaveName =  bindingClass.tvEdTextNamePerson
        editSaveSecondName = bindingClass.tvEdTextSecondName
        editSaveFatherName = bindingClass.tvEdTextFather
        editSaveAge = bindingClass.tvEdTextAge
        editSaveHobby = bindingClass.tvEdTextHobby
        bindingClass.tvTextDataName.setText(sharedPref.getString("name", "Имя:"))
        bindingClass.tvTextDataSecondName.setText(sharedPref.getString("secondName", "Фамилия:"))
        bindingClass.tvTextDataFatherName.setText(sharedPref.getString("fatherName", "Отчество:"))
        bindingClass.tvTextDataAge.setText(sharedPref.getString("age", "Возраст:"))
        bindingClass.tvTextDataHobby.setText(sharedPref.getString("hobby", "Хобби:"))
    }

    fun OnClickMenu(view: View){
        bindingClass.tvEdTextNamePerson.visibility = View.INVISIBLE
        bindingClass.tvEdTextSecondName.visibility = View.INVISIBLE
        bindingClass.tvEdTextFather.visibility = View.INVISIBLE
        bindingClass.tvEdTextAge.visibility = View.INVISIBLE
        bindingClass.tvEdTextHobby.visibility = View.INVISIBLE
        bindingClass.tvTextDataName.visibility = View.INVISIBLE
        bindingClass.tvTextDataSecondName.visibility = View.INVISIBLE
        bindingClass.tvTextDataFatherName.visibility = View.INVISIBLE
        bindingClass.tvTextDataFatherName.visibility = View.INVISIBLE
        bindingClass.tvTextDataAge.visibility = View.INVISIBLE
        bindingClass.tvTextDataHobby.visibility = View.INVISIBLE
        bindingClass.buttonSave.visibility = View.VISIBLE
        bindingClass.buttonChange.visibility = View.VISIBLE
        bindingClass.buttonGoToActivity.visibility = View.VISIBLE
    }

    fun OnClickChageBackground(view: View){
        var randomVal = Random()
        viewModel.currentColor.value = Color.argb(255,randomVal.nextInt(256), randomVal.nextInt(256),
            randomVal.nextInt(256))
        viewModel.currentColor.value = viewModel.color
    }
    fun OnClickGoToActivity(view: View){
        if(isInfoRight()) {
            val intent = Intent(this, NewActivity::class.java)
            intent.putExtra("name", bindingClass.tvEdTextNamePerson.text.toString())
            intent.putExtra("age", bindingClass.tvEdTextAge.text.toString().toInt())
            intent.putExtra("secondName", bindingClass.tvEdTextSecondName.text.toString())
            intent.putExtra("fatherName", bindingClass.tvEdTextFather.text.toString())
            intent.putExtra("hobby", bindingClass.tvEdTextHobby.text.toString())
            intent.putExtra("backgroundColor", viewModel.currentColor.value)
            startActivity(intent)

        }else{
            bindingClass.tvTextError.visibility = View.VISIBLE
        }
    }
    fun OnClickBack(view: View){
        bindingClass.tvEdTextNamePerson.visibility = View.VISIBLE
        bindingClass.tvEdTextSecondName.visibility = View.VISIBLE
        bindingClass.tvEdTextFather.visibility = View.VISIBLE
        bindingClass.tvEdTextAge.visibility = View.VISIBLE
        bindingClass.tvEdTextHobby.visibility = View.VISIBLE
        bindingClass.tvTextDataName.visibility = View.VISIBLE
        bindingClass.tvTextDataSecondName.visibility = View.VISIBLE
        bindingClass.tvTextDataFatherName.visibility = View.VISIBLE
        bindingClass.tvTextDataFatherName.visibility = View.VISIBLE
        bindingClass.tvTextDataAge.visibility = View.VISIBLE
        bindingClass.tvTextDataHobby.visibility = View.VISIBLE
        bindingClass.buttonSave.visibility = View.INVISIBLE
        bindingClass.buttonChange.visibility = View.INVISIBLE
        bindingClass.buttonGoToActivity.visibility = View.INVISIBLE
    }

    fun isInfoRight() : Boolean{
       var name = bindingClass.tvEdTextNamePerson.text.toString()
       var secondName = bindingClass.tvEdTextSecondName.text.toString()
       var fatherName = bindingClass.tvEdTextFather.text.toString()
       var age = bindingClass.tvEdTextAge.text.toString()
       var hobby = bindingClass.tvEdTextHobby.text.toString()

        return !(name.isEmpty() || secondName.isEmpty()
                || fatherName.isEmpty() || age.isEmpty() || hobby.isEmpty())
    }

    fun onClickButtonToInfo(view: View){
        if(isInfoRight()) {
            val intent = Intent(this, InfoActivity::class.java)
            intent.putExtra("name", bindingClass.tvEdTextNamePerson.text.toString())
            intent.putExtra("age", bindingClass.tvEdTextAge.text.toString().toInt())
            intent.putExtra("secondName", bindingClass.tvEdTextSecondName.text.toString())
            intent.putExtra("fatherName", bindingClass.tvEdTextFather.text.toString())
            intent.putExtra("hobby", bindingClass.tvEdTextHobby.text.toString())
            intent.putExtra("backgroundColor", viewModel.currentColor.value)
            startActivity(intent)

        }else{
            bindingClass.tvTextError.visibility = View.VISIBLE
        }
    }
    fun getBackgroundColorFromActivity(){
        var backgroundColor = intent.getIntExtra("backgroundColor", Color.WHITE)
        bindingClass.tvBackground.setBackgroundColor(backgroundColor)
        viewModel.currentColor.value = backgroundColor
    }
}
